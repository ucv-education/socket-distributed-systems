import op_file
import glossary
import json
import utils
import time
import random
import log


#
def read_bd_file():
    return op_file.read_file(glossary.ANSWERS_FILE)


#
def search_all():
    all_records = read_bd_file()

    if all_records == None:
        return []

    return all_records


#
def search_title_awswers_survey(title):
    all_records = search_all()
    filtered=[]
    for record in all_records:
        if record[glossary.TITULO] == title:
            filtered.append(record)

    if len(filtered) == 0:
        return None
    else:
        return filtered


#
def search_title_login_awswers_survey(title, login):
    all_records = search_all()
    for record in all_records:
        if record[glossary.TITULO] == title and record[glossary.LOGIN_FIELD] == login:
            return record

    return None


#
def add_update_answer(data):

    answers_to_title_login = search_title_login_awswers_survey(
        data[glossary.TITULO], data[glossary.LOGIN_FIELD])
    answers_to_title = search_title_awswers_survey(data[glossary.TITULO])
    
    all_data=search_all()

    result_to_save = {
        glossary.PREGUNTA: data[glossary.PREGUNTA],
        glossary.RESPUESTA: data[glossary.RESPUESTA]
    }

    if answers_to_title == None or answers_to_title_login == None:

        record_title = {
            glossary.TITULO: data[glossary.TITULO],
            glossary.LOGIN_FIELD: glossary.LOGIN,
            glossary.RESULTADOS: [result_to_save]
        }

        if answers_to_title == None:
            op_file.write_file([record_title], glossary.ANSWERS_FILE)
            return data

        else:

            all_data.append(record_title)
            op_file.write_file(all_data, glossary.ANSWERS_FILE)
            return data

    else:

        data_filtered = []
        for record in all_data:
            if record[glossary.LOGIN_FIELD] == glossary.LOGIN:
                if not record[glossary.TITULO] == data[glossary.TITULO]:
                    data_filtered.append(record)
                else:
                    record[glossary.RESULTADOS].append(result_to_save)
                    data_filtered.append(record)
            else:
                data_filtered.append(record)

        op_file.write_file(data_filtered, glossary.ANSWERS_FILE)
        return data
