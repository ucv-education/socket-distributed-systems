import errno
import select
import socket
import json
import psutil
import sys
import struct
import time
from multiprocessing import Process, Manager, Value
#
import multicast_inbound
import log
import menu_client_employee
import utils
import glossary
import overlap_network
import op_file

log.debug('stating client employee....')

op_file.delete_config_files()

glossary.DATA_NODE = {
    "hostname": socket.gethostname(),
    "address": socket.gethostbyname(socket.gethostname()),
    "type": glossary.TYPE_APPLICATION
}


run_multicast_inbound = Process(
    target=multicast_inbound.run_multicast_inbound, args=())

run_decrease_counter_nodes = Process(
    target=overlap_network.decrease_counter_nodes, args=())

run_multicast_inbound.start()
time.sleep(1)
run_decrease_counter_nodes.start()

time.sleep(1)

menu_client_employee.control()

run_multicast_inbound.join()
run_decrease_counter_nodes.join()
