import time
import glossary
from datetime import datetime


def msg(msg):
    print("")
    print(msg)
    time.sleep(3)


def get_datetime():
    return datetime.now()


def set_datetime(str_date):
    if not glossary.TYPE_APPLICATION == glossary.SERVER:
        glossary.DATE_TIME_CLIENT = datetime.strptime(
            str_date, glossary.FORMAT_DATE)


def dtm_to_str(dtm):
    return dtm.strftime(glossary.FORMAT_DATE)

def str_to_dtm(str_date):
    return datetime.strptime(str_date, glossary.FORMAT_DATE)
