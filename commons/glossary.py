import os
import sys
from datetime import datetime


FORMAT_DATE='%d/%m/%y %H:%M:%S'
DATE_TIME_CLIENT=datetime.now()

JSON_EXTENSION=".json"
ENCRYPT_EXTENSION=".encrypt"

PRE_INSCRIPCION="pre_inscripion"
LLENADO="llenado"

INDIQUE_OPCION_VALIDA='Indique una opcion valida: '
SELECCIONE_UNA_OPCION='Seleccione una opcion: '
CONTINUAR_OPCION='Seleccioe cualquier tecla para continuar'


NAME_APPLICATION=sys.argv[1]
TYPE_APPLICATION=sys.argv[2]
WORKDIR_APPLICATION=sys.argv[3]
LOG_APPLICATION=sys.argv[4]

SERVER_DAEMON="server_daemon"
CLIENT_ADMIN="client_admin"
CLIENT_EMPLOYEE="client_employee"
#
# Overlap Network
PORT_MULTICAST = int(sys.argv[5])

ADDRESS_MULTICAST = '224.3.29.71'
BD_FILE="bd_surveys.json"
BD_EMPLOYEE="bd_employee.json"
OVERLAP_NETWORK_FILE="overlap_network.json"
INBOUND_OUTBOUND_FILE="inbound_outbound.json"

ANSWERS_FILE="answers.json"

#
BUFFER_SIZE=4096
NODE_TTL=3

#
DATA_NODE={}

#

REQUEST="request"
RESPONSE="response"
COMMAND_RETRY_TIME=3


SALIR_TEXT="0.- Salir"
PRESIONE_CUALQUIER_TECLA="Presione cualquier tecla para continuar....."

STATUS_PREINSCRIPCION="No se puede modificar encuesta esta en periodo de preinscripcion desde el "
STATUS_ESPERANDO_LLENADO="No se puede modificar encuesta, finalizo periodo de preinscripcion, esperando inicio de llenado el "
STATUS_LLENADO="No se puede modificar encuesta, Esta en periodo de llenado, desde el "
ESTATUS_LLENADO_FINALIZADO="Encuesta finalizo, el periodo de llenado era del " 


LOGIN=None
PASSWORD=None

TITULO = "titulo"
STATUS = "status"
KEY = "key"
DATETIME="datetime"
EMPLEADOS = "empleados"
DATA_EMPLEADO = "data_empleado"
RESULTADOS = "resultados"
PREGUNTA = "pregunta"
RESPUESTA = "respuesta"

LOGIN_FIELD = "login"
EMAIL_FIELD = "email"
NOMBRE_FIELD = "nombre"
TELEFONO_FIELD = "telefono"
DEPARTAMENTO_FIELD = "departamento"
PASSWORD_FIELD = "password"



