import op_file
import glossary
import json
import utils
import time
import random
import log

def read_overlap_network_info():
    return op_file.read_file(glossary.OVERLAP_NETWORK_FILE)

def exist_file_overlap_network():
    return not op_file.read_file(glossary.OVERLAP_NETWORK_FILE)==None

def add_update_node(new_node):
    
    new_node["ttl"]=glossary.NODE_TTL
    
    nodes = read_overlap_network_info()
    sw=False
    if nodes == None:
        nodes=[new_node]
    else:
        for node in nodes:
            if node["hostname"] == new_node["hostname"] and node["address"] == new_node["address"] and node["tcp_port_inbound"] == new_node["tcp_port_inbound"]:
                node["datetime"]=utils.get_datetime().strftime(glossary.FORMAT_DATE)
                node["ttl"]=glossary.NODE_TTL
                sw=True
                break
        if not sw:
            nodes.append(new_node)
            
       
    op_file.write_file(nodes,glossary.OVERLAP_NETWORK_FILE)
    
#
#
#
def decrease_counter_nodes():
    try:
        while True:
            nodes_filtered=[]
            nodes = read_overlap_network_info()
            if not nodes == None:
                for node in nodes:
                    node["ttl"]=int(node["ttl"])-1
                    if int(node["ttl"])>0:
                        nodes_filtered.append(node)
                op_file.write_file(nodes_filtered,glossary.OVERLAP_NETWORK_FILE)
            time.sleep(glossary.NODE_TTL)
            
    except Exception:
        log.error("Se cayo el deminio decrease_counter_nodes en typo applicacion " + glossary.TYPE_APPLICATION)
        
def print_servers():
    nodes = read_overlap_network_info()
    if nodes == None:
        return
    else:
        for node in nodes:
            print(node)
    print("")
            
            
def get_enable_server():
    nodes = read_overlap_network_info()
    index=-1
    if  not nodes==None:
        if len(nodes) >= 1:
            index=random.randint(0, len(nodes)-1)
        if (index>=0):
            return nodes[index]
    return index


    
