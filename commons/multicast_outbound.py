import socket
import sys
import struct
import time
import json
import encrypt
import log
import glossary
import datetime
import utils
import overlap_network
import bd_functions_survey

#
#
def run(data_node):
    log.debug(" multicast outbound: up")
    multicast_group = (glossary.ADDRESS_MULTICAST, glossary.PORT_MULTICAST)

    #
    # Create the datagram socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Set a timeout so the socket does not block indefinitely when trying
    # to receive data.
    sock.settimeout(0.2)

    # Set the time-to-live for messages to 1 so they do not go past the
    # local network segment. ..cd cl

    ttl = struct.pack('b', glossary.NODE_TTL)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

    while True:

        try:
            
            surveys_ended=[]
            
            all_data = bd_functions_survey.search_all()
            for survey in all_data:
                if bd_functions_survey.survey_ended_not_storage_data(survey):
                    surveys_ended.append(survey)
                    bd_functions_survey.add_update_survey(survey,1)
            

            message = build_message(sock,surveys_ended, data_node["tcp_port_inbound"])


            # Send data to the multicast group
            log.debug('multicast outbound: sending "%s"' % message)
            log.debug("multicast outbound: token " +
                      str(encrypt.encrypt_message(message)))
            sock.sendto(encrypt.encrypt_message(message), multicast_group)

            # Look for responses from all recipients
            while True:
                log.debug('multicast outbound: waiting to receive')
                try:
                    token, server = sock.recvfrom(glossary.BUFFER_SIZE)
                    log.debug(
                        'multicast outbound: recibido (token) ' + str(token))
                    data = encrypt.decrypt_token(token)
                    
                    log.debug(
                        'multicast outbound: recibido (data) ' + data)
                    
                    data_json=json.loads(data)
                    # Actualizar surveys
                    for survey in data_json["surveys_ended"]:
                        bd_functions_survey.add_answers_to_survey(survey)
                    
                except socket.timeout:
                    log.debug(
                        'multicast outbound: timed out, no more responses')
                    break
                else:
                    log.debug('multicast outbound: received "%s" from %s' %
                              (data, server))

        except KeyboardInterrupt:
            break

        finally:
            time.sleep(5)

    sock.close()

#
#
#
def build_message(sock, surveys_ended,tcp_port_inbound):
    message = {
        "hostname": socket.gethostname(), 
        "address": socket.gethostbyname(socket.gethostname()),
        "datetime": utils.get_datetime().strftime(glossary.FORMAT_DATE),
        "type": glossary.TYPE_APPLICATION,
        "surveys_ended": surveys_ended,
        "tcp_port_inbound": tcp_port_inbound,
        "ttl": glossary.NODE_TTL
    }
    return json.dumps(message)
