import json
import socket
import struct
import time
import encrypt
import log
import glossary
import utils
import overlap_network
import bd_functions_answers

#
def run_multicast_inbound():

    log.debug("multicast inbound up")

    ANY = "0.0.0.0"
    MCAST_ADDR = glossary.ADDRESS_MULTICAST
    MCAST_PORT = glossary.PORT_MULTICAST

    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)

    # Allow multiple sockets to use the same PORT number
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Bind to the port that we know will receive multicast data
    sock.bind((ANY, MCAST_PORT))

    # Tell the kernel that we are a multicast socket
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 255)

    # Tell the kernel that we want to add ourselves to a multicast group
    # The address for the multicast group is the third param
    sock.setsockopt(socket.IPPROTO_IP,    socket.IP_ADD_MEMBERSHIP,
                             socket.inet_aton(MCAST_ADDR) + socket.inet_aton(ANY))

    # Receive/respond loop
    while True:
        log.debug('Multicast_inbound: Esperando para recibir un mensaje')
        token, address = sock.recvfrom(glossary.BUFFER_SIZE)
        data = encrypt.decrypt_token(token)
        log.debug('Multicast_inbound: data recibida ' + data)
        log.debug('Multicast_inbound: recibido %s bytes de %s' %
                  (len(data), address))

        # Agrega servidor si es nuevo, de lo contrario actualiza informacion del server, solo en 
        # el cliente administrador y el de empleado
        
        data_json=json.loads(data)
        
        overlap_network.add_update_node(data_json)
        
        # Verify ended surveys
        
        surveys_ended=[]
        for survey in data_json['surveys_ended']:
            s=bd_functions_answers.search_title_awswers_survey(survey[glossary.TITULO])
            if not s==None:
                surveys_ended.append(s)

        log.debug('Multicast_inbound: enviando acknowledgement')
        msg = build_message(sock,surveys_ended)
        sock.sendto(encrypt.encrypt_message(msg), address)

#
def build_message(sock,surveys_ended):
    message = {
        "hostname": socket.gethostname(),
        "address": socket.gethostbyname(socket.gethostname()),
        "datetime": utils.get_datetime().strftime(glossary.FORMAT_DATE),
        "surveys_ended": surveys_ended,
        "type": glossary.TYPE_APPLICATION,
    }
    return json.dumps(message)
