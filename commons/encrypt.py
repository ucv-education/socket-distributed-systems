from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import base64
import os

def generate_key():
    password = b"password"
    salt_value=b'\xf6M\xb2\xbd<\xff"-^\xf2}Q\'\x01X~'    
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt_value,
        iterations=100000,
    )
    key = base64.urlsafe_b64encode(kdf.derive(password))
    return  Fernet(key)

def encrypt_message(msg):
    if type(msg) == bytes:
        return generate_key().encrypt(msg)
    if type(msg) == str:
        return generate_key().encrypt(msg.encode())
    return ""
    
def decrypt_token(token):
    if type(token) == bytes:
        return generate_key().decrypt(token).decode()
    if type(token) == str:
        return generate_key().decrypt(token).decode()    