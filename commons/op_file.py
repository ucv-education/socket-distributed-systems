import json
import os
import time
import socket
from os import listdir
from os.path import isfile, join
import encrypt
from datetime import datetime
from utils import msg
import glossary
from datetime import timedelta
import utils


#
def get_path_of_file(filename):
    return join(glossary.WORKDIR_APPLICATION, filename)

#
def exist_file(filename):
    return os.path.exists(get_path_of_file(filename))

#
def is_survey_file(filename):
    try:
        if int(len(read_file(filename)["titulo"]))>0:
            return True
        else:
            return False
    except Exception:
        return False

#
def write_file(new_data, filename):
    #
    if type(new_data) == str or type(new_data) == list or type(new_data) == dict:
        with open(get_path_of_file(filename), 'w', encoding='utf-8') as f:
            json.dump(new_data, f, ensure_ascii=False, indent=4)
    if type(new_data) == bytes:
        with open(get_path_of_file(filename), 'wb') as f:
            f.write(new_data)

#
def read_file(filename, type_return=dict):
    data_loaded = None
    try:
        with open(get_path_of_file(filename)) as data_file:
            if(type_return == dict):
                data_loaded = json.load(data_file)
            else:
                return data_file
    except Exception:
        data_loaded
    return data_loaded

#
def list_files():
    files = [f for f in listdir(glossary.WORKDIR_APPLICATION) if isfile(
        join(glossary.WORKDIR_APPLICATION, f)) and f.find(glossary.JSON_EXTENSION)]
    return files

# Quitar
def survey_already_load(filename):
   return exist_file(filename) and read_file(filename).__contains__('id')
       

#
def delete_config_files():
    filename = glossary.OVERLAP_NETWORK_FILE
    if exist_file(filename):
        os.remove(get_path_of_file(filename))
    filename = glossary.INBOUND_OUTBOUND_FILE
    if exist_file(filename):
        os.remove(get_path_of_file(filename))

#
def status_survey_to_clients(f):
    data_loaded = read_file(f)

    # Si no ha comenzado la pre-inscripcion
    inicio_pre_incripcion = datetime.strptime(
        data_loaded["inicio_pre_incripcion"], glossary.FORMAT_DATE)
    fin_pre_inscripcion = inicio_pre_incripcion + \
        timedelta(minutes=int(data_loaded["duracion_pre_incripcion"]))

    inicio_llenado = datetime.strptime(
        data_loaded["inicio_llenado"], glossary.FORMAT_DATE)
    fin_llenado = inicio_pre_incripcion + \
        timedelta(minutes=int(data_loaded["duracion_llenado"]))

    now = datetime.now()

    if now >= inicio_pre_incripcion and now <= fin_pre_inscripcion:
        return glossary.PRE_INSCRIPCION

    if now >= inicio_llenado and now <= fin_llenado:
        return glossary.LLENADO

    return None    
