import os
import sys


# Commands

COMMAND_SEARCH_ALL_SURVEY="search_all"
COMMAND_SEARCH_SURVEY="search_survey"
COMMAND_EXIST_SURVEY="exist_survey"
COMMAND_ADD_UPDATE_SURVEY="add_update_survey"
COMMAND_LIST_ENABLE_SURVEY_FOR_PRE_INSCRIPCION="list_enable_survey_for_pre_incripcion"
COMMAND_PREINSCRIPCION="preinscripcion"
COMMAND_LIST_ENABLE_SURVEY_FOR_FILL="list_enable_survey_for_fill"
COMMAND_START_FILL="start_fill"

COMMAND_ADD_UPDATE_ANSWER="add_update_answer"

COMMAND_DATETIME_SERVER="datetime_server"

COMMAND_HOW_MUCH_TIME_LEFT="how_much_time_left"


COMMAND_SEARCH_ALL_EMPLOYEE="search_all"
COMMAND_SEARCH_EMPLOYEE="search_employee"
COMMAND_EXIST_EMPLOYEE="exist_employee"
COMMAND_ADD_UPDATE_EMPLOYEE="add_update_employee"

COMMAND_CHECK_LOGIN_IN="check_login_in"



COMMAND_RESPONSE="ACK"