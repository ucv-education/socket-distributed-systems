import socket
import sys
import time
import overlap_network
import log
import glossary
import json
import random
import utils
import commands
import encrypt

#
def request(command, data_for_send):
    request = {
        "id":  random.randrange(0, 10000000),
        "name:": glossary.NAME_APPLICATION,
        "datetime": utils.get_datetime().strftime(glossary.FORMAT_DATE),
        "command": command,
        "data": data_for_send,
        "initiator": {
            "hostname": glossary.DATA_NODE["hostname"],
            "address": glossary.DATA_NODE["address"],
            "type": glossary.DATA_NODE["type"],
            "tcp_port_inbound": -1
        }
    }
    return run(request, glossary.REQUEST)

#
def response(request, data_replay, port_response):
    request["command"] = commands.COMMAND_RESPONSE
    request["sender_ack"] = {
        "hostname": glossary.DATA_NODE["hostname"],
        "address": glossary.DATA_NODE["address"],
        "type": glossary.DATA_NODE["type"],
        "tcp_port_inbound": glossary.DATA_NODE["tcp_port_inbound"]
    }
    return run(response, glossary.RESPONSE)


#
def run(data_for_send, type_operation):
    retry = glossary.COMMAND_RETRY_TIME
    while retry > 0:
        result=execute(data_for_send, type_operation)
        if result["code"]==500:
            retry=retry-1
            time.sleep(1)
        else:
            return result["data"]
        
    return None


#
def execute(data_for_send, type_operation):

    try:

        data_received = None
        server = overlap_network.get_enable_server()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

        tcp_port_destiny = -1

        # Si el destino es un cliente
        if type_operation == glossary.RESPONSE:
            data_for_send["sender_ack"]["tcp_port_inbound"] = server["tcp_port_inbound"]
            tcp_port_destiny = data_for_send["initiator"]["tcp_port_inbound"]
        else:
            tcp_port_destiny = server["tcp_port_inbound"]

        sock.connect(('', tcp_port_destiny))
        sock.setblocking(True)

        # Si el destino es un servidor
        if type_operation == glossary.REQUEST:
            data_for_send["initiator"]["tcp_port_inbound"] = sock.getsockname()[
                1]

        log.info("Send: " + json.dumps(data_for_send))
        
        
        msg_encrypt=encrypt.encrypt_message(json.dumps(data_for_send))
        sock.send(msg_encrypt)
        token = sock.recv(glossary.BUFFER_SIZE)
        data_received=json.loads(encrypt.decrypt_token(token))

        log.info("Received: " + str(data_received))
    except Exception:
        sock.close()
        return {
            "code": 500
        }
    finally:
        sock.close()

    if not data_received==None and len(data_received)>0:
        return {
            "code": 200,
            "data": data_received
        }
        
    return {
        "code": 200,
        "data": None
    }