import op_file
import glossary
import json
import utils
import time
import random
import log


#
def read_bd_file():
    return op_file.read_file(glossary.BD_EMPLOYEE)


#
def search_all():
    all_records = read_bd_file()

    if all_records == None:
        return []

    return all_records


#
def search_employee(login):
    all_records = search_all()
    for record in all_records:
        if record[glossary.LOGIN_FIELD] == login:
            return record

    return None


#
def exist_employee(login):
    all_records = search_all()

    for record in all_records:
        if record[glossary.LOGIN_FIELD] == login:
            return True

    return False


#
def add_update_employee(data):

    all_data = search_all()
    if not exist_employee(data[glossary.LOGIN_FIELD]):
        all_data.append(data)
        op_file.write_file(all_data, glossary.BD_EMPLOYEE)
        return data
    else:

        all_data_filtered = []

        for record in all_data:
            if not record[glossary.LOGIN_FIELD] == data[glossary.LOGIN_FIELD]:
                all_data_filtered.append(record)

        all_data_filtered.append(data)
        op_file.write_file(all_data_filtered, glossary.BD_EMPLOYEE)
        return data


#
def check_login_in(data):

    if not exist_employee(data[glossary.LOGIN_FIELD]):
        return {
            "code": 401,
            "msg": "Usuario no autorizado"
        }
    else:
        all_data = search_all()
        for record in all_data:
            if record[glossary.LOGIN_FIELD] == data[glossary.LOGIN_FIELD] and record[glossary.PASSWORD_FIELD] == data[glossary.PASSWORD_FIELD]:
                return {
                    "code": 200,
                    "data": record
                }

    return {
        "code": 401,
        "msg": "Usuario no autorizado"
    }
