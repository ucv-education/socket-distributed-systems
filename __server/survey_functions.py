import os
import sys
import log
import time
import json
import encrypt
import op_file
from os.path import isfile, join
from termcolor import colored
from datetime import datetime
from datetime import timedelta
import glossary
from utils import msg
import random
from os import listdir
import encrypt
import op_file
import utils



#
def check_surveys(extension):
    #
    onlyfiles = op_file.list_files()
    #
    for f in onlyfiles:
        
        if not op_file.is_survey_file(f): continue
        

    return onlyfiles

# for server response
def get_surveys():
    files = op_file.list_files()
    for file in files:
        data_json = op_file.read_file(file)
        msgs=[]
        verify_fechas_after_load(data_json, msgs)
        file["msgs"]=msgs
    return files

#
def save_survey(name_file):
    data_loaded = op_file.read_file(name_file)
    id_survey = random.randrange(0, 10000000)
    data_loaded.update({'id': id_survey})
    op_file.write_file(data_loaded, name_file)
    msg("Encuesta Cargada...")

#
def verify_survey(filename,data):
    msgs = []
    verify_titulo(filename, data, msgs)
    verify_descripcion(filename, data, msgs)
    verify_before_begin(data, msgs)
    verify_preguntas(filename, data, msgs)
    
    if len(msgs) == 0:
        return []
    
    return msgs

#
def verify_titulo(f, data_loaded, msgs):
    if check_basic_about_attribute(data_loaded, msgs, "titulo") == True and len(data_loaded["titulo"]) < 10:
        msgs.append("El titulo debe tener minimo diez(10) caracteres")

#
def verify_descripcion(f, data_loaded, msgs):
    if check_basic_about_attribute(data_loaded, msgs, "descripcion") == True and len(data_loaded["descripcion"]) < 10:
        msgs.append(
            "La descripcion debe tener minimo diez(10) caracteres")

#
def verify_before_begin(data_loaded, msgs):

    if check_basic_about_attribute(data_loaded, msgs, "inicio_pre_incripcion") == True:
        inicio_pre_incripcion = ""
        try:
            inicio_pre_incripcion = datetime.strptime(
                data_loaded["inicio_pre_incripcion"], glossary.FORMAT_DATE)
        except Exception:
            msgs.append(
                "La fecha para el inicio de a pre-inscripcion debe tener el formato " + glossary.FORMAT_DATE)
            return

        if inicio_pre_incripcion <= utils.get_datetime():
            msgs.append(
                "La fecha de pre-inscripcion debe ser mayor a la fecha/hora actual")


    if check_basic_about_attribute(data_loaded, msgs, "duracion_pre_incripcion") == True:
        try:
            minutes = int(data_loaded["duracion_pre_incripcion"])
            if (minutes < 1):
                msgs.append(
                    "Debe indicar un valor entero mayor de cero(0) minutos para la duracion de la pre-incripcion")
        except Exception:
            msgs.append(
                "Debe indicar un valor entero en minutos para la duracion de la pre-incripcion")


    if check_basic_about_attribute(data_loaded, msgs, "inicio_pre_incripcion") == True:

        inicio_llenado = ""
        try:

            inicio_llenado = datetime.strptime(
                data_loaded["inicio_llenado"], glossary.FORMAT_DATE)

            if inicio_llenado <= utils.get_datetime():
                msgs.append(
                    "La fecha de inicio de llenado debe ser mayor a la fecha/hora actual")

        except Exception:
            msgs.append(
                "La fecha inicio de llenado debe tener el formato " + glossary.FORMAT_DATE)

    if check_basic_about_attribute(data_loaded, msgs, "duracion_llenado") == True:
        try:
            minutes = int(data_loaded["duracion_llenado"])
            if (minutes < 0):
                msgs.append(
                    "Debe indicar un valor entero mayor de cero(1) minutos para la duracion del llenado")

        except Exception:
            msgs.append(
                "Debe indicar un valor entero en minutos para la duracion del llenado")


    if check_basic_about_attribute(data_loaded, msgs, "tiempo_visualizacion_respuesta_de_pregunta") == True:
        try:
            minutes = int(data_loaded["tiempo_visualizacion_respuesta_de_pregunta"])
            if (minutes < 2):
                msgs.append(
                    "Debe indicar un valor entero mayor de un (1) segundo para mostrar la respuesta una vez respondida la pregunta")

        except Exception:
            msgs.append(
                "Debe indicar un valor entero mayor de un (1) segundo para mostrar la respuesta una vez respondida la pregunta")
            
            
    if check_basic_about_attribute(data_loaded, msgs, "tiempo_para_responder_la_encuesta") == True:
        try:
            minutes = int(data_loaded["tiempo_para_responder_la_encuesta"])
            if (minutes < 5):
                msgs.append(
                    "Debe indicar un valor entero mayor de un (5) minutos para el tiempo de duracion para responder la encuesta")

        except Exception:
            msgs.append(
                "Debe indicar un valor entero mayor de un (5) minutos para el tiempo de duracion para responder la encuesta")
            
            

#
def survey_ready_check_when_begin(data_loaded,msgs):
    now = datetime.now()
    inicio_pre_incripcion = datetime.strptime(
        data_loaded["inicio_pre_incripcion"], glossary.FORMAT_DATE)
    fin_pre_inscripcion = inicio_pre_incripcion + \
        timedelta(minutes=int(data_loaded["duracion_pre_incripcion"]))
    if now < inicio_pre_incripcion:
        msgs.append("Encuesta sin problemas, no ha comenzado, el periodo de pre-inscripcion es, " +
                    utils.dtm_to_str(inicio_pre_incripcion) + " al " + utils.dtm_to_str(fin_pre_inscripcion))
    
#
def verify_fechas_after_load(data_loaded, msgs):

    inicio_pre_incripcion = datetime.strptime(
        data_loaded["inicio_pre_incripcion"], glossary.FORMAT_DATE)
    fin_pre_inscripcion = inicio_pre_incripcion + \
        timedelta(minutes=int(data_loaded["duracion_pre_incripcion"]))

    inicio_llenado = datetime.strptime(
        data_loaded["inicio_llenado"], glossary.FORMAT_DATE)
    fin_llenado = inicio_llenado + \
        timedelta(minutes=int(data_loaded["duracion_llenado"]))

    now = datetime.now()

    if now >= inicio_pre_incripcion and now <= fin_pre_inscripcion:
        msgs.append(glossary.STATUS_PREINSCRIPCION +
                    utils.dtm_to_str(inicio_pre_incripcion) + " al " + utils.dtm_to_str(fin_pre_inscripcion))
        return

    if now > fin_pre_inscripcion and now < inicio_llenado:
        msgs.append(glossary.STATUS_ESPERANDO_LLENADO +
                    utils.dtm_to_str(inicio_llenado))
        return

    if now >= inicio_llenado and now <= fin_llenado:
        msgs.append(glossary.STATUS_LLENADO +
                    utils.dtm_to_str(inicio_llenado) + " al " + utils.dtm_to_str(fin_llenado))
        return

    if now >= fin_llenado:
        msgs.append(glossary.ESTATUS_LLENADO_FINALIZADO +
                    utils.dtm_to_str(inicio_llenado) + " al " + utils.dtm_to_str(fin_llenado))


#
def verify_preguntas(f, data_loaded, msgs):

    if not "preguntas" in data_loaded.keys() or not type(data_loaded["preguntas"]) == list:
        msgs.append(
            "Debe indicar la lista de preguntas")
    else:
        if len(data_loaded["preguntas"]) == 0:
            msgs.append(
                "Debe indicar como minimo una (1) pregunta")
        else:
            for p in data_loaded["preguntas"]:
                if not type(p) == str:
                    msgs.append(
                        "La pregunta '" + str(p) + "' debe ser un texto")

#
def check_basic_about_attribute(data_loaded, msgs, attr):
    if not attr in data_loaded.keys():
        msgs.append("Debe indicar el atributo " + attr + " en la encuesta")
        return False
    else:
        if data_loaded[attr] == None:
            msgs.append(msgs.append(
                "Debe indicar un valor en el atributo " + attr + " en la encuesta"))
            return False
    return True

#
def get_text_item_file(list_files,file_data):
    return  "Opcion(x): " + str(list_files.index(file_data)+1) + "| Archivo: " + file_data["filename"] + \
        " | Titulo encuesta: " + file_data["data"]["titulo"]
        
#
def print_msg_of_file(msgs, namefile):
    if type(msgs) == list:
        for msg in msgs:
            print(colored("    " + msg, "red"))
        
        
#
def exist_status_in_msgs(msgs,txt):
    for i in msgs:
        if txt in i:
            return True
    return False

