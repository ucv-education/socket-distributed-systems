import socket
import time
from multiprocessing import Process, Manager, Value
import logging
import multicast_outbound
import log
import glossary
import overlap_network
import op_file
import daemon
import bd_functions_survey

def run():
    
    log.info("Arrancando servidor....")
    
    op_file.delete_config_files()

    data_node = Manager().dict()
    
    run_server = Process(target=daemon.run, args=(data_node,))
    
    run_multicast_outbound = Process(
        target=multicast_outbound.run, args=(data_node,))
    
    run_decrease_counter_nodes = Process(
        target=overlap_network.decrease_counter_nodes, args=())


    run_server.start()
    time.sleep(1)
    run_multicast_outbound.start()
    time.sleep(1)
    run_decrease_counter_nodes.start()
    
    time.sleep(1)
    glossary.DATA_NODE = data_node

    run_server.join()
    run_multicast_outbound.join()
    run_decrease_counter_nodes.join()
    