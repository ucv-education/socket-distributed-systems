import socket
import sys
import struct
import encrypt

import op_file
import glossary
import json
import utils
import time
import random
import log
import survey_functions
from datetime import timedelta


#
def read_bd_file():
    return op_file.read_file(glossary.BD_FILE)


#
def search_all():
    all_records = read_bd_file()

    if all_records == None:
        return []

    for record in all_records:
        set_status_survey(record)

    return all_records


#
def search_survey(title):
    all_records = search_all()
    for record in all_records:
        if record[glossary.TITULO] == title:
            set_status_survey(record)
            return record

    return None


#
def list_enable_survey_for_pre_incripcion():
    all_records = search_all()
    surveys_filtered = []
    for record in all_records:
        set_status_survey(record)
        for msg in record["status"]:
            if msg.find(glossary. STATUS_PREINSCRIPCION) >= 0:
                surveys_filtered.append(record)
    return surveys_filtered


#
def list_enable_survey_for_fill(data):
    all_records = search_all()
    surveys_filtered = []
    for record in all_records:
        set_status_survey(record)
        for msg in record["status"]:

            employee = {
                glossary.LOGIN_FIELD: data[glossary.LOGIN_FIELD],
                glossary.KEY: -1
            }

            if msg.find(glossary. STATUS_LLENADO) >= 0 and employee in record[glossary.EMPLEADOS]:
                surveys_filtered.append(record)

    return surveys_filtered


#
def exist_survey(title):
    all_records = search_all()

    for record in all_records:
        if record[glossary.TITULO] == title:
            return True

    return False


#
def set_status_survey(record):
    msgs = []
    survey_functions.verify_fechas_after_load(record, msgs)
    record[glossary.STATUS] = msgs


#
def add_update_survey(data, sw=0):
    all_data = search_all()
    if not exist_survey(data[glossary.TITULO]):
        all_data.append(data)
        op_file.write_file(all_data, glossary.BD_FILE)
        return data
    else:

        # if el status > 0 no puede modificarse la encuesta solo si el sw=0 (el defecto)

        survey_in_bd = search_survey(data[glossary.TITULO])

        if sw == 0 and len(survey_in_bd[glossary.STATUS]) > 0:
            return survey_in_bd

        all_data_filtered = []

        for record in all_data:
            if not record[glossary.TITULO] == data[glossary.TITULO]:
                all_data_filtered.append(survey_in_bd)

        all_data_filtered.append(data)
        op_file.write_file(all_data_filtered, glossary.BD_FILE)
        return data


#
def preinscripcion(data):

    survey = search_survey(data[glossary.TITULO])
    employee = {
        glossary.LOGIN_FIELD: data[glossary.LOGIN_FIELD],
        glossary.KEY: -1
    }

    if not survey.__contains__(glossary.EMPLEADOS):
        survey[glossary.EMPLEADOS] = [employee]
    else:
        survey[glossary.EMPLEADOS].append(employee)

    return add_employee_to_survey(survey)


#
def add_employee_to_survey(data):

    survey_in_bd = search_survey(data[glossary.TITULO])
    all_data_filtered = []

    all_data = search_all()
    for record in all_data:
        if not record[glossary.TITULO] == data[glossary.TITULO]:
            all_data_filtered.append(survey_in_bd)

    all_data_filtered.append(data)
    op_file.write_file(all_data_filtered, glossary.BD_FILE)
    return data


#
def start_fill(data):

    employee = {
        glossary.LOGIN_FIELD: data[glossary.LOGIN_FIELD],
        glossary.KEY: -1
    }

    record_to_return = None
    all_data_filtered = []

    all_data = search_all()
    for record in all_data:
        if record[glossary.TITULO] == data[glossary.TITULO] and employee in record[glossary.EMPLEADOS]:
            for emp in record[glossary.EMPLEADOS]:
                if emp[glossary.LOGIN_FIELD] == data[glossary.LOGIN_FIELD]:
                    emp[glossary.KEY] = random.randrange(100, 999)
                    emp[glossary.DATETIME] = utils.dtm_to_str(
                        utils.get_datetime())
                    record_to_return = emp
                    all_data_filtered.append(record)
                    break
        else:
            all_data_filtered.append(record)

    op_file.write_file(all_data_filtered, glossary.BD_FILE)

    return record_to_return


#
def survey_ended_not_storage_data(survey):
    for msg in survey[glossary.STATUS]:
        if msg.find(glossary.ESTATUS_LLENADO_FINALIZADO) >= 0:
            return True
    return False


def contain_answers(survey):

    for employee in survey[glossary.EMPLEADOS]:
        if employee.__contains__[glossary.RESPUESTAS]:
            return True

    return False


def add_answers_to_survey(surveys):

    all_data = search_all()

    for record in all_data:
        for survey in surveys:
            if record[glossary.TITULO] == survey[glossary.TITULO]:
                for emp in record[glossary.EMPLEADOS]:
                    if emp[glossary.LOGIN_FIELD] == survey[glossary.LOGIN_FIELD]:
                        emp[glossary.RESULTADOS] = survey[glossary.RESULTADOS]
                        break

    op_file.write_file(all_data, glossary.BD_FILE)

    return all_data


def how_much_time_left(data):
    all_data = search_all()

    for record in all_data:
        if record[glossary.TITULO] == data[glossary.TITULO]:
            for emp in record[glossary.EMPLEADOS]:
                if emp[glossary.LOGIN_FIELD] == data[glossary.LOGIN_FIELD]:
                    time_delta = utils.str_to_dtm(emp[glossary.DATETIME]) + timedelta(
                        minutes=record["tiempo_para_responder_la_encuesta"]) - utils.get_datetime()
                    minutes = int(time_delta.total_seconds() / 60)
                    return {
                        "minutes_left": minutes
                    }

    return None
