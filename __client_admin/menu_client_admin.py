import os
import sys
import log
import time
import json
import op_file
import survey_functions
from termcolor import colored
from os.path import isfile, join
from utils import msg
import glossary
import pprint
import overlap_network
import request_response
import commands

#


def control():
    while True:
    
        head("Encuestas en BD")
        list_surveys = request_response.request(commands.COMMAND_SEARCH_ALL_SURVEY, "")
        
        
        if  list_surveys == None or len(list_surveys) == 0:
            print(colored("Noy hay encuestas en la BD","red"))
            print("")
            print(colored("0.- Cargar encuesta", "green"))
            opcion = seleccione()

            if not opcion == 0:
                continue
            
            if opcion==0: 
                menu_load_survey()
            
        
        else:            
            
            print("")
            for file_detail in list_surveys:
                print(colored(str(list_surveys.index(file_detail)+1) + " - " + file_detail["titulo"] + " - " + str(file_detail["status"]),"blue"))
            print("")
            
            print(colored("0.- Cargar encuesta", "green"))
            print(colored("x.- Indique el numero de encuesta a consultar", "green"))
            opcion = seleccione()

            if opcion < 0 or opcion > len(list_surveys):
                continue
            
            if opcion==0: 
                menu_load_survey()
                continue
        
            os.system('clear')
            print(colored("Detalle de la encuesta","blue"))
            print(colored("-----------------------------------------","blue"))
            print("")
            pprint.pprint(list_surveys[opcion-1])
            print("")
            input(glossary.PRESIONE_CUALQUIER_TECLA)

        
#
def menu_load_survey():
    while True:

        head("Cargar archivo desde el directorio de trabajo")

        list_json_files = op_file.list_files()
        list_surveys = []
        for filename in list_json_files:
            if op_file.is_survey_file(filename):
                list_surveys.append({
                    "filename": filename,
                    "data": op_file.read_file(filename)
                })

        if len(list_surveys) > 0:
            print("")
            for file_detail in list_surveys:
                filename = file_detail["filename"]
                data = file_detail["data"]
                msgs = survey_functions.verify_survey(filename, data)
                file_detail["msgs"] = msgs
                print(colored(survey_functions.get_text_item_file(
                    list_surveys, file_detail), "green"))
                survey_functions.print_msg_of_file(msgs, filename)
            print("")
            print(colored(glossary.SALIR_TEXT, "green"))
            print(colored("x.- Indique el numero de archivo a cargar", "green"))
            opcion = seleccione()

            if opcion < 0 or opcion > len(list_surveys):
                continue
            
            if opcion==0: break
            
            if (len(list_surveys[opcion-1]["msgs"])) > 0:
                os.system('clear')
                print("")
                print("")
                print(colored("Este archivo posee observaciones, no puede ser cargado","red"))
                input(glossary.PRESIONE_CUALQUIER_TECLA)
            else:
                
                list_surveys[opcion-1]["data"][glossary.EMPLEADOS]=[]
                
                result=request_response.request(commands.COMMAND_ADD_UPDATE_SURVEY, list_surveys[opcion-1]["data"])
                
                if result==None:
                    os.system('clear')
                    print(colored("Ocurrio un problema, informacion no actualizada","red"))
                    print("")
                    input(glossary.PRESIONE_CUALQUIER_TECLA)
                    continue
                
                if (result.__contains__('status')):
                    os.system('clear')
                    print(colored("Alerta: No se puede modificar la encuesta (ver informacion en BD","red"))
                    print(colored("-----------------------------------------","red"))
                    print(result[glossary.STATUS])
                    print(colored("-----------------------------------------","red"))
                    print("")
                    pprint.pprint(result)
                    print("")
                    print(colored("La encuesta no puede ser modificada, lea el status!","red"))
                    print("")
                    input(glossary.PRESIONE_CUALQUIER_TECLA)
                    break
                
                print(colored("Informacion actualizada","blue"))
                input(glossary.PRESIONE_CUALQUIER_TECLA)
                break
                
                
                
                    
                
                    

        else:
            
            print("")
            print(colored(
                "Para tener encuestas disponibles para cargar al sistema cree un archivo .json", "yellow"))
            print(colored(
                "en el directorio de trabajo (workspace) indicado, vea los ejemplos para conocer", "yellow"))
            print(colored(
                "el formato para construirlo. El sistema lo leera y lo validara para ud.", "yellow"))
            print("")
            input(colored(glossary.PRESIONE_CUALQUIER_TECLA, "green"))
            break
        
#
def menu_consultar_surveys():
    while True:

        head("Consultar encuestas en BD")
        list_surveys = request_response.request(commands.COMMAND_SEARCH_ALL_SURVEY, "")
        
        
        if  list_surveys == None or len(list_surveys) == 0:
            os.system('clear')
            print(colored("Noy hay datos en la BD","red"))
            print("")
            input(glossary.PRESIONE_CUALQUIER_TECLA)
            break
            
        else:
            
            print("")
            for file_detail in list_surveys:
                print(colored(str(list_surveys.index(file_detail)+1) + " - " + file_detail["titulo"] + " - " + str(file_detail["status"]),"blue"))
            print("")
            print(colored(glossary.SALIR_TEXT, "green"))
            print(colored("x.- Indique el numero de encuestas a consultar", "green"))
            opcion = seleccione()

            if opcion < 0 or opcion > len(list_surveys):
                continue
            
            if opcion==0: break
            
            os.system('clear')
            print(colored("Detalle de la encuesta","blue"))
            print(colored("-----------------------------------------","blue"))
            print("")
            pprint.pprint(list_surveys[opcion-1])
            print("")
            input(glossary.PRESIONE_CUALQUIER_TECLA)

        

#
def head(title):
    os.system('clear')

    while overlap_network.get_enable_server() == -1:
        print("Esperando por servidor")
        time.sleep(1)
    os.system('clear')

    print("")
    print(colored("Autor: Elbrick Salazar, Proyecto Sistemas Distribuidos", "blue"))
    print("")

    print("Directorio de trabajo actual: " + glossary.WORKDIR_APPLICATION)
    print("")
    print(colored(title, "green"))
    print(colored("----------------------------------------------", "green"))

#
def seleccione():
    opcion = input(glossary.SELECCIONE_UNA_OPCION)
    opcion_number = -1
    try:
        opcion_number = int(opcion)
    except Exception:
        opcion_number = -1
    return opcion_number

#
def log_show_error(error, e):
    log.debug(error + str(e))
    msg(error + str(e))

#
def print_servers():
    time.sleep(1)
    nodes = overlap_network.read_overlap_network_info()
    if not nodes == None:
        for node in nodes:
            print(node)
